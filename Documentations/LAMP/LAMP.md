# **<center>Mise en place d'un serveur LAMP</center>**
  
### Les tâches que j'ai effectuées pour ce projet sont :  
  
*  **Installer les logiciels "MariaDB", "Apache" et "PHP7" sur le serveur Debian**

**Installation de MariaDB :**

```SSH
apt-get -y install mariadb-server
```
Répondre "**Oui**" si on demande d’installer les dépendances ou des mises à jour. 

```SSH
mysql_secure_installation
```
Pour sécuriser l'installation et répondre "**Y**" jusqu'à la fin, puis changer le mot de passe de l'utilisateur "**root**".

```SSH
mysql –u root–p 
```

![](LAMP1.png) </br>

Pour vérifier le bon fonctionnement de "**MySQL**".

```SSH
show databases;
```

![](LAMP2.png) </br>

On arrive sur "**MariaDB**" et cette commande permet de visualiser les bases créées dans "**MariaDB**".

```SSH
exit;
```
Pour sortir de "**MariaDB**".

</br>

**Installation d'Apache :**

```SSH
apt-get install apache2
```
Répondre "**Oui**" si on demande d’installer les dépendances ou des mises à jour. 

Ouvrir un navigateur web sur le poste physique, puis saisir « http://IpDuServeurLamp ».

![](LAMP3.png) </br>

Un message "**It Works**" indiquant qu'Apache 2 est fonctionnel.

</br>

**Installation de PHP7 :**

```SSH
apt-get –y install php php-common
```
Répondre "**Oui**" si on demande d’installer les dépendances ou des mises à jour. 

```SSH
php-v
```
Pour vérifier la version de "**PHP**" installée.

```SSH
apt-get -y install php-cli php-fpm php-json php-pdo php-mysql php-zip php-gd php-mbstringphp-curlphp-xmlphp-pearphp-bcmath
```
Pour installer des extensions de "**PHP**".

```SSH
apt-get -y install libapache2-mod-php
```
Pour fournir le module "**PHP**" pour le serveur web "**Apache 2**".

```SSH
a2enmod php7.3
```
Pour activer le module "**PHP 7.3**"" dans "**Apache**".

```SSH
apachectl configtest
```
Pour vérifier que la configuration d’"**Apache**" est correcte avant le redémarrage.

```SSH
systemctl restart apache2
```
Pour redémarrer le service "**Apache**".

```PHP
<?php 
    phpinfo();  
?> 
```
Tester que le serveur web interprète les fichiers "**PHP**", placer dans le dossier "**/var/www/html**" le fichier suivant : "**test.php**".

![](LAMP4.png) </br>

Saisir dans le navigateur « http://IpDuServeurLamp/test.php ».

</br>

*  **Mettre en place le protocole FTP pour accéder à distance au serveur WebLab**

**Installation de ProFTPd :**

```SSH
apt-get install proftpd
```
Pour installer "**ProFTPd**" et répondre "**Oui**".

```SSH
nano /etc/proftpd/proftpd.conf
```
![](LAMP5.png) </br>

![](LAMP6.png) </br>

![](LAMP7.png) </br>

Pour modifier le fichier de configuration de "**ProFTPd**".

```SSH
/etc/init.d/proftpd restart
```
Pour le redémarrage du service "**ProFTPd**" après la nouvelle configuration.

</br>

**Création d'utilisateurs FTP :**

```SSH
adduser adminsite
```
Pour ajouter un utilisateur sur le serveur "**FTP**".

```SSH
adduser devel
```
Pour ajouter un autre utilisateur sur le serveur "**FTP**".

```SSH
addgroup ftpadmin
```
Pour créer un groupe sur le serveur "**FTP**".

```SQL
usermod –g ftpadmin adminsite
```
Pour ajouter l'utilisateur "**adminsite**" dans le groupe "**ftpadmin**".

```SQL
ls –al /var/www
```
Pour lister les droits que contiennent les répertoires dans "**/var/www**".

```SQL
chown –R adminsite:ftpadmin /var/www/html
```
Pour changer l’utilisateur principal "**adminsite**" et le groupe "**ftpadmin**".

```SQL
ls –l /var/www
```
Pour voir si l’utilisateur et le groupe "**root**" est changé par "**adminsite**" et "**ftpadmin**".

```SQL
chmod 775 /var/www/html/gestfrais
```
Pour donner les droits au répertoire "**gestfrais**".

</br>

*  **Créer un hôte DNS afin de faire une recherche par le nom d'hôte**

**Créations des hôtes sur le DNS :**

Créer un nouvel hôte dans la zone de recherche direct gsb7.sio nommer gestfrais avec comme adresse IP 192.172.1.13 

![](LAMP8.png) </br>

</br>

* **Configurer un hôte virtuel pour gérer la configuration du site Web**

```SQL
mkdir gestfrais /var/www/html
```
Pour créer un répertoire "**gestfrais**" dans "**/var/www/html/**".

```SQL
nano /etc/apache2/sites-available/gestfrais.gsb7.sio.conf
```
![](LAMP9.png) </br>

Pour créer le fichier de configuration.

```SQL
apache2ctl configtest
```
Pour vérifier que la configuration d’"**Apache**" est correcte avant le redémarrage.

```SQL
a2ensite gestfrais.gsb7.sio
```
Pour créer un lien symbolique du fichier de "**sites-available/**" vers "**sites-enabled/**".

```SQL
service apache2 reload
```
Pour activer le site et recharger "**Apache**". 

Saisir dans le navigateur "**http://gestfrais.gsb7.sio**", pour tester avec un navigateur si l’hôte virtuel fonctionne.

</br>

* **Mise en place du site Web développé par les développeurs dans les fichiers de configuration du serveur**

**Pour accéder en FTP au serveur WebLab :**

![](LAMP10.png) </br>

1 : Correspond à l’adresse IP du serveur "**WebLab**". 

2 : Correspond à l’identifiant du serveur on peut utiliser soit "**adminsite**" ou "**devel**". 

3 : Correspond au mot de passe. 

4 : Correspond au port qui est "**21**". 

![](LAMP11.png) </br>

On se retrouve dans le dossier "**gestfrais**" créer dans le serveur "**LAMP**" dans "**/var/www/html/gestfrais**", pour y mettre les dossiers "**PHP**" et "**MySQL**" du groupe SLAM afin que l’on puisse afficher le site web GSB.

</br>

**Pour se connecter en SSH depuis un outil client :**

![](LAMP12.png) </br>

1 : On clique sur l’onglet "**session**". 

2 : On clique sur l’onglet "**SSH**".

![](LAMP13.png) </br>

1 : On entre l’adresse du serveur "**LAMP**". 

2 : On entre le nom d’utilisateur "**userssh**" avec son mot de passe. 

![](LAMP14.png) </br>

On peut voir que l’on est connecté en "**userssh**" depuis un outil client.