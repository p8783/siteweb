# **<center>Mise en place d'Eyes OF Networks</center>**
  
### Les tâches que j'ai effectuées pour ce projet sont :  
  
**Paramétrage d'EON :**

Lancement de la machine virtuelle "**EON**".

![](EON1.png) </br>

Entrer la commande "**nmtui**" pour démarrer le menu d'interface graphique.

Aller sur "**Modifier une connexion**".

![](EON2.png) </br>

Puis "**<Modifier...>**".

![](EON3.png) </br>

Mettre la machine sur le réseau.

![](EON4.png) </br>

Entrer la commande "**ip a**" pour voir la configuration de la carte réseau.

Il va falloir modifier le fichier "**snmpd.conf**" du serveur EON pour prendre en compte la modification de la community.

"**vi /etc/snmp/snmpd.conf**" descendre jusqu’aux lignes :
```SSH
#       sec.name  source          community
com2sec notConfigUser  default EyesOfNetwork
```

Pour changer cette ligne, se placer sur "**EyesOfNetwork**", supprimer ce nom de community.
Saisir le nouveau nom de community : "**gsb**"

```SSH
com2sec notConfigUser  default gsb
```

Sortir du mode insert, appuyer sur la touche "**INSERT**".
Saisir  "**:w**", puis valider pour sauvegarder les modifications .
Saisir  "**:q**", puis valider pour sortir de vi. 
Relancer le service "**snmpd**".
```SSH
Systemctl restart snmpd
```

Se rendre sur l’adresse "**192.177.1.75**" ou le DNS "**eon.gsb7.sio**".

![](EON5.png) </br>

![](EON6.png) </br>

</br>

* **Supervision du serveur LAMP**
  
**Paramétrage de l'hôte :**

Aller sur l’onglet "**Administration**", puis "**Configuration Nagios**".
En haut de la page cliquer sur "**Equipement**", puis "**Ajouter**".

![](EON7.png) </br>

Entrer les informations.

![](EON8.png) </br>

Aller dans l’onglet "**Checks**" et modifier les informations si nécessaires.

![](EON9.png) </br>

Aller dans l’onglet "**Outils**", puis "**Appliquer la configuration**".

![](EON10.png) </br>

Vérifier le bon fonctionnement.

![](EON11.png) </br>

</br>

**Paramétrage MySQL :**

Aller sur l’onglet "**Administration**", puis "**Configuration Nagios**".
En haut de la page cliquer sur "**Equipement**", puis "**Lister**".

![](EON12.png) </br>

Aller sur "**Serveur LAMP**".

![](EON13.png) </br>

Aller dans l’onglet "**Services**", puis cliquer sur "**Create A New Service For This Host**".

Cliquer sur "**Add Service**".

![](EON14.png) </br>

Aller dans "**Services**", cliquer sur le service puis aller dans "**Checks**" et modifier si nécessaires.

![](EON15.png) </br>

Aller dans l’onglet "**Outils**", puis "**Appliquer la configuration**".

![](EON16.png) </br>

Vérifier le bon fonctionnement.

![](EON17.png) </br>

</br>

**Paramétrage d'Apache :**

Aller sur l’onglet "**Administration**", puis "**Configuration Nagios**".
En haut de la page cliquer sur "**Equipement**", puis "**Lister**".

Aller sur "**Serveur LAMP**".

Aller dans l’onglet "**Services**", puis cliquer sur "**Create A New Service For This Host**".

Cliquer sur "**Add Service**".

Aller dans "**Services**", cliquer sur le service puis aller dans "**Checks**" et modifier si nécessaires.

Aller dans l’onglet "**Outils**", puis "**Appliquer la configuration**".

Vérifier le bon fonctionnement.

</br>

**Paramétrage de SSH :**

Aller sur l’onglet "**Administration**", puis "**Configuration Nagios**".
En haut de la page cliquer sur "**Equipement**", puis "**Lister**".

Aller sur "**Serveur LAMP**".

Aller dans l’onglet "**Services**", puis cliquer sur "**Create A New Service For This Host**".

Cliquer sur "**Add Service**".

Aller dans "**Services**", cliquer sur le service puis aller dans "**Checks**" et modifier si nécessaires.

Aller dans l’onglet "**Outils**", puis "**Appliquer la configuration**".

Vérifier le bon fonctionnement.

</br>

**Paramétrage de FTP :**

Aller sur l’onglet "**Administration**", puis "**Configuration Nagios**".
En haut de la page cliquer sur "**Equipement**", puis "**Lister**".

Aller sur "**Serveur LAMP**".

Aller dans l’onglet "**Services**", puis cliquer sur "**Create A New Service For This Host**".

Cliquer sur "**Add Service**".

Aller dans "**Services**", cliquer sur le service puis aller dans "**Checks**" et modifier si nécessaires.

Aller dans l’onglet "**Outils**", puis "**Appliquer la configuration**".

Vérifier le bon fonctionnement.

**Résultat final :**

![](EON18.png) </br>

</br>

* **Supervision d'un routeur**

**Paramétrage :**

Mettre une adresse IP à un port du routeur.

![](EON19.png) </br>

Mettre "**snmp-server community gsb RO**" pour superviser la community.

![](EON20.png) </br>

On ajoute le routeur dans "**Equipement**" et "**Ajouter**".

Entrer les informations avec l’adresse IP, sélectionner "**Cisco**" dans "**Add Template To Inherit From**", puis cliquer sur "**Add Host**".

Aller dans l’onglet "**Checks**" et modifier les informations si nécessaires.

Aller dans l’onglet "**Outils**", puis "**Appliquer la configuration**".

Vérifier le bon fonctionnement.

**Résultat final :**

![](EON21.png) </br>

</br>

* **Supervision d'un switch**

**Paramétrage :**

Créer un vlan, lui donner une adresse IP et affecter les ports au vlan.

Mettre "**snmp-server community gsb RO**" pour superviser la community.

![](EON22.png) </br>

On ajoute le switch dans "**Equipement**" et "**Ajouter**".

Entrer les informations avec l’adresse IP, sélectionner "**Cisco**" dans "**Add Template To Inherit From**", puis cliquer sur "**Add Host**".

Aller dans l’onglet "**Checks**" et modifier les informations si nécessaires.

Aller dans l’onglet "**Outils**", puis "**Appliquer la configuration**".

Vérifier le bon fonctionnement.

**Résultat final :**

![](EON23.png) </br>