# **<center>Mise en place de mon site web sur GitLab</center>**
  
### Les tâches que j'ai effectuées pour ce projet sont :

**Préparation de la machine virtuelle**

Démarrer la machine virtuelle

```SSH
mkdir siteweb
```
Création du répertoire "**siteweb**" pour pouvoir déposer les fichiers.

```SSH
cd siteweb
```
Se déplacer dans le répertoire "**siteweb**".

```SSH
git init
```
Initialisation du projet "**GitLab**".

</br>
  
**Création du projet sur GitLab**

![](GIT1.png) </br>
Aller sur "**Create new project**".

![](GIT2.png) </br>
Aller sur "**Create blank project**".

![](GIT3.png) </br>

![](GIT4.png) </br>

Entrer les informations importantes.

</br>

**Connexion en SSH sur Visual Studio Code**

Lancer l'application "**Visual Studio Code**" et télécharger l'extension "**Remote - SSH**".

![](GIT7.png) </br>
Aller dans l'onglet "**Explorateur distant**" et entrer l'adresse IP avec son chemin.

![](GIT5.png) </br>
Entrer le mot de passe.

![](GIT6.png) </br>
Réalisation du site web.

</br>

**Envoyer son projet sur GitLab**

```SSH
git status
```
Voir les fichiers qui ont été modifiés.

```SSH
git add .
```
Pour indexer les fichiers modifiers et les préparer au commit

```SSH
git commit -m "Ajout de mon site web sur GitLab"
```
Capture des changements.

```SSH
git remote rename origin old-origin
```
Renommer le fichier "**origin**" par "**old-origin**" sauf si c'est la première fois.

```SSH
git remote add origin git@gitlab.com:p8783/siteweb.git
```
Pour centraliser le code source au projet.

```SSH
git push -u origin --all
```
Envoie du code sur "**GitLab**".

Vous pouvez retrouvez ce projet
[ici](https://gitlab.com/p8783/siteweb).